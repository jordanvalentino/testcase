import 'package:majootestcase/app.dart';
import 'package:majootestcase/utils/global.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  database = openDatabase(
    join(await getDatabasesPath(), 'testcase.db'),
    onCreate: (db, version) {
      return db.execute(
        'CREATE TABLE user(email TEXT PRIMARY KEY, password TEXT, username TEXT)',
      );
    },
    version: 1,
  );

  runApp(MyApp());
}
