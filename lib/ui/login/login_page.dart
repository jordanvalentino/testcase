import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user/user.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  static const routeName = '/login';

  final _loginEmailController = TextController();
  final _loginPasswordController = TextController();
  final _registerEmailController = TextController();
  final _registerUsernameController = TextController();
  final _registerPasswordController = TextController();
  final _registerRepeatPasswordController = TextController();

  GlobalKey<FormState> loginFormKey = new GlobalKey<FormState>();
  GlobalKey<FormState> registerFormKey = new GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  bool _isObscurePassword = true;
  bool _isObscureRepeatPassword = true;

  String _currentForm = 'login';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBlocCubit, AuthBlocState>(
      listener: (context, state) {
        if (state is AuthBlocLoggedInState) {
          Navigator.pushReplacementNamed(context, '/home');
        }
      },
      child: SafeArea(
        child: Scaffold(
          key: scaffoldKey,
          body: Builder(
            builder: (context) => SingleChildScrollView(
              child: Padding(
                padding:
                    EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Selamat Datang!',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        // color: colorBlue,
                      ),
                    ),
                    Text(
                      'Silahkan masuk/daftar terlebih dahulu.',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    _currentForm == 'login'
                        ? _loginFormField()
                        : _registerFormField(),
                    SizedBox(
                      height: 50,
                    ),
                    CustomButton(
                      text: _currentForm == 'login' ? 'Masuk' : 'Daftar',
                      onPressed: () {
                        _currentForm == 'login'
                            ? _handleLogin(
                                context.bloc<AuthBlocCubit>(),
                                context,
                              )
                            : _handleRegister(
                                context.bloc<AuthBlocCubit>(),
                                context,
                              );
                      },
                      height: 100,
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    _currentForm == 'login'
                        ? _registerFormLink()
                        : _loginFormLink(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _showSnackbar(BuildContext context, String content) {
    Scaffold.of(context).hideCurrentSnackBar();
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(content),
      ),
    );
  }

  Widget _loginFormField() {
    return Form(
      key: loginFormKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _loginEmailController,
            isEmail: true,
            label: 'Email',
            hint: 'e.g. example@mail.com',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val)
                    ? null
                    : 'Masukkan e-mail yang valid.';
              return null;
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'e.g. 1234',
            controller: _loginPasswordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _registerFormField() {
    return Form(
      key: registerFormKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _registerEmailController,
            isEmail: true,
            label: 'Email',
            hint: 'example@mail.com',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val)
                    ? null
                    : 'Masukkan e-mail yang valid.';
              return null;
            },
          ),
          CustomTextFormField(
            context: context,
            controller: _registerUsernameController,
            isEmail: true,
            label: 'Username',
            hint: 'example_username',
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'e.g. 1234',
            controller: _registerPasswordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            label: 'Repeat Password',
            hint: 'e.g. 1234',
            controller: _registerRepeatPasswordController,
            isObscureText: _isObscureRepeatPassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscureRepeatPassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscureRepeatPassword = !_isObscureRepeatPassword;
                });
              },
            ),
            validator: (val) {
              final isRepeated = val == _registerPasswordController.value;
              if (val != null)
                return isRepeated ? null : 'Password tidak sama.';
              return null;
            },
          ),
        ],
      ),
    );
  }

  Widget _registerFormLink() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          setState(() {
            _loginEmailController.textController.clear();
            _loginPasswordController.textController.clear();
            _currentForm = 'register';
          });
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  Widget _loginFormLink() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          setState(() {
            _registerEmailController.textController.clear();
            _registerUsernameController.textController.clear();
            _registerPasswordController.textController.clear();
            _registerRepeatPasswordController.textController.clear();
            _currentForm = 'login';
          });
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Masuk',
                ),
              ]),
        ),
      ),
    );
  }

  void _handleLogin(AuthBlocCubit authBloc, BuildContext context) async {
    final isValid = loginFormKey.currentState.validate();

    final email = _loginEmailController.value ?? '';
    final password = _loginPasswordController.value ?? '';

    if (email.isNotEmpty && password.isNotEmpty) {
      if (isValid) {
        User user = User(
          email: email,
          password: password,
        );

        await authBloc.authUser(user).then((value) async {
          if (value) {
            _showSnackbar(context, "Login berhasil.");
            Future.delayed(Duration(seconds: 2))
                .then((_) => authBloc.loginUser(user));
          } else
            _showSnackbar(
                context, "Login gagal, periksa kembali inputan anda.");
        });
      }
    } else {
      _showSnackbar(context,
          "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan.");
    }
  }

  void _handleRegister(AuthBlocCubit authBloc, BuildContext context) async {
    final isValid = registerFormKey.currentState?.validate();

    final email = _registerEmailController.value ?? '';
    final username = _registerUsernameController.value ?? '';
    final password = _registerPasswordController.value ?? '';
    final repeatPassword = _registerRepeatPasswordController.value ?? '';

    if (email.isNotEmpty &&
        username.isNotEmpty &&
        password.isNotEmpty &&
        repeatPassword.isNotEmpty) {
      if (isValid && password == repeatPassword) {
        User user = User(
          email: email,
          userName: username,
          password: password,
        );

        await authBloc.registerUser(user).then((value) {
          if (value) {
            _showSnackbar(context, "Register berhasil.");
            Future.delayed(Duration(seconds: 2))
                .then((_) => authBloc.loginUser(user));
          } else
            _showSnackbar(
                context, "Register gagal, periksa kembali inputan anda.");
        });
      }
    } else {
      _showSnackbar(context,
          "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan.");
    }
  }
}
