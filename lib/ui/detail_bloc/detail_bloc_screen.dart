import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/detail_bloc/detail_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/detail_bloc/detail_bloc_loaded_screen.dart';
import 'package:majootestcase/ui/extra/error_notfound_screen.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/extra/loading.dart';

class DetailBlocScreen extends StatelessWidget {
  static const String routeName = '/detail';

  const DetailBlocScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthBlocCubit>.value(
      value: context.bloc<AuthBlocCubit>(),
      child: BlocBuilder<DetailBlocCubit, DetailBlocState>(
          builder: (context, state) {
        if (state is DetailBlocInitialState) {
          return _initialStateWidget();
        } else if (state is DetailBlocLoadingState) {
          return _loadingStateWidget();
        } else if (state is DetailBlocLoadedState) {
          return _loadedStateWidget(state);
        } else if (state is DetailBlocErrorState) {
          return _errorStateWidget(state);
        }

        // print("state not implemented $state");
        return ErrorNotFoundScreen();
      }),
    );
  }

  Widget _initialStateWidget() => LoadingIndicator();

  Widget _loadingStateWidget() => LoadingIndicator();

  Widget _loadedStateWidget(DetailBlocLoadedState state) =>
      DetailBlocLoadedScreen(movieData: state.data);

  Widget _errorStateWidget(DetailBlocErrorState state) =>
      ErrorScreen(message: state.error);
}

class DetailBlocScreenArguments {
  final HomeBlocCubit homeBlocCubit;
  final String movieId;

  DetailBlocScreenArguments(
      {@required this.homeBlocCubit, @required this.movieId});
}
