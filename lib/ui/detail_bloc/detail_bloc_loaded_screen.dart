import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie/movie_data.dart';
import 'package:majootestcase/models/movie/movie_series.dart';

class DetailBlocLoadedScreen extends StatefulWidget {
  final MovieData movieData;

  const DetailBlocLoadedScreen({Key key, this.movieData}) : super(key: key);

  @override
  _DetailBlocLoadedScreenState createState() =>
      _DetailBlocLoadedScreenState(movieData);
}

class _DetailBlocLoadedScreenState extends State<DetailBlocLoadedScreen> {
  MovieData _movieData;

  _DetailBlocLoadedScreenState(this._movieData);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text('${_movieData.title} (${_movieData.year})'),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.network(_movieData.image.imageUrl),
            SizedBox(height: 20),
            Text(
              "${_movieData.title} (${_movieData.year})",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            SizedBox(height: 50),
            (_movieData.series != null && _movieData.series?.length > 0)
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Series List",
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(height: 20),
                        SizedBox(
                          height: 250,
                          child: ListView.builder(
                            // physics: NeverScrollableScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: _movieData.series?.length ?? 0,
                            itemBuilder: (context, index) => SizedBox(
                              width: 250,
                              child: _movieSeriesItemWidget(
                                  _movieData.series[index]),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    ));
  }

  Widget _movieSeriesItemWidget(MovieSeries data) {
    return Card(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(25.0),
        ),
      ),
      child: Column(
        children: [
          Image.network(
            data.image.imageUrl,
            fit: BoxFit.cover,
            height: 150,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Text(
              data.title,
              textDirection: TextDirection.ltr,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }
}
