import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/detail_bloc/detail_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/detail_bloc/detail_bloc_screen.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/index_screen.dart';

class AppRouter {
  AuthBlocCubit authBlocCubit = AuthBlocCubit();

  Route onGenerateRoutes(RouteSettings settings) {
    switch (settings.name) {
      case IndexScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => BlocProvider.value(
            value: authBlocCubit..fetchHistoryLogin(),
            child: IndexScreen(),
          ),
        );
        break;
      case HomeBlocScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<AuthBlocCubit>.value(
                value: authBlocCubit,
              ),
              BlocProvider<HomeBlocCubit>(
                create: (context) => HomeBlocCubit()..fetchData(),
              ),
            ],
            child: HomeBlocScreen(),
          ),
        );
        break;
      case DetailBlocScreen.routeName:
        final args = settings.arguments as DetailBlocScreenArguments;

        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<AuthBlocCubit>.value(
                value: authBlocCubit,
              ),
              BlocProvider<HomeBlocCubit>.value(
                value: args.homeBlocCubit,
              ),
              BlocProvider<DetailBlocCubit>(
                create: (context) => DetailBlocCubit(
                  args.homeBlocCubit.movieResponse,
                  args.movieId,
                )..fetchMovieData(),
              ),
            ],
            child: DetailBlocScreen(),
          ),
        );
        break;
      default:
        return null;
    }
  }
}
