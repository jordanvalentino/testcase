import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/extra/error_notfound_screen.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';

class IndexScreen extends StatelessWidget {
  static const routeName = '/';

  const IndexScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(builder: (context, state) {
      if (state is AuthBlocInitialState) {
        return _initialStateWidget();
      } else if (state is AuthBlocLoadingState) {
        return _loadingStateWidget();
      } else if (state is AuthBlocLoginState) {
        return _loginStateWidget(context.bloc<AuthBlocCubit>());
      } else if (state is AuthBlocLoggedInState) {
        return _loggedInStateWidget(context.bloc<AuthBlocCubit>());
      } else if (state is AuthBlocErrorState) {
        return _errorStateWidget(state);
      }

      // print("state not implemented $state");
      return ErrorNotFoundScreen();
    });
  }

  Widget _initialStateWidget() => Scaffold();

  Widget _loadingStateWidget() => LoadingIndicator();

  BlocProvider _loginStateWidget(AuthBlocCubit authBlocCubit) =>
      BlocProvider.value(
        value: authBlocCubit,
        child: LoginPage(),
      );

  MultiBlocProvider _loggedInStateWidget(AuthBlocCubit authBlocCubit) =>
      MultiBlocProvider(
        providers: [
          BlocProvider<AuthBlocCubit>.value(
            value: authBlocCubit,
          ),
          BlocProvider<HomeBlocCubit>(
            create: (context) => HomeBlocCubit()..fetchData(),
          )
        ],
        child: HomeBlocScreen(),
      );

  Widget _errorStateWidget(AuthBlocErrorState state) =>
      ErrorScreen(message: state.error);
}
