import 'package:flutter/material.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';

import 'error_screen.dart';

class ErrorConnectionScreen extends StatelessWidget {
  final String message;
  final IconData icon;
  final Function() retry;

  const ErrorConnectionScreen({
    Key key,
    this.message = "No internet connection.",
    this.icon = Icons.signal_wifi_off_rounded,
    @required this.retry,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ErrorScreen(
      message: message,
      icon: icon,
      retry: retry,
      retryButton: ErrorConnectionRetryButton(retry: retry),
    );
  }
}

class ErrorConnectionRetryButton extends StatelessWidget {
  final Function retry;

  const ErrorConnectionRetryButton({
    Key key,
    @required this.retry,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: retry,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.refresh),
          Text("REFRESH"),
        ],
      ),
    );
  }
}
