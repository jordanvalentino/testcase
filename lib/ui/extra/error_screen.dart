import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final IconData icon;
  final double gap;
  final Function() retry;
  final Widget retryButton;
  final double fontSize;
  final Color textColor;

  const ErrorScreen({
    Key key,
    this.message = "Unknown error occured.",
    this.icon = Icons.warning_rounded,
    this.gap = 50,
    this.retry,
    this.retryButton,
    this.fontSize = 14,
    this.textColor = Colors.black,
  })  : assert(message != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(icon),
            Text(
              message,
              style: TextStyle(
                fontSize: fontSize,
                color: textColor,
              ),
            ),
            retry != null
                ? Column(
                    children: [
                      SizedBox(
                        height: gap,
                      ),
                      retryButton ??
                          IconButton(
                            onPressed: retry ?? () {},
                            icon: Icon(Icons.refresh_rounded),
                          ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
