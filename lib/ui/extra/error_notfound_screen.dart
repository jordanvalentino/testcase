import 'package:flutter/material.dart';

import 'error_screen.dart';

class ErrorNotFoundScreen extends StatelessWidget {
  final String message;
  final IconData icon;
  final Function() retry;

  const ErrorNotFoundScreen({
    Key key,
    this.message = "Page not found.",
    this.icon = Icons.android_rounded,
    this.retry,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ErrorScreen(
      message: message,
      icon: icon,
      retry: retry ?? null,
    );
  }
}
