import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/extra/error_connection_screen.dart';
import 'package:majootestcase/ui/extra/error_notfound_screen.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_loaded_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  static const routeName = '/home';

  const HomeBlocScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthBlocCubit>.value(
      value: context.bloc<AuthBlocCubit>(),
      child:
          BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
        if (state is HomeBlocInitialState) {
          return _initialStateWidget();
        } else if (state is HomeBlocLoadedState) {
          return _loadedStateWidget(state);
        } else if (state is HomeBlocLoadingState) {
          return _loadingStateWidget();
        } else if (state is HomeBlocErrorConnectionState) {
          return _errorConnectionStateWidget(
              context.bloc<HomeBlocCubit>().fetchData);
        } else if (state is HomeBlocErrorState) {
          return _errorStateWidget(state);
        }

        // print("state not implemented $state");
        return ErrorNotFoundScreen();
      }),
    );
  }

  Widget _initialStateWidget() => LoadingIndicator();

  Widget _loadingStateWidget() => LoadingIndicator();

  Widget _loadedStateWidget(HomeBlocLoadedState state) =>
      HomeBlocLoadedScreen(listMovieData: state.data);

  Widget _errorConnectionStateWidget(Function retry) =>
      ErrorConnectionScreen(retry: retry);

  Widget _errorStateWidget(HomeBlocErrorState state) =>
      ErrorScreen(message: state.error);
}
