import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie/movie_data.dart';
import 'package:majootestcase/ui/detail_bloc/detail_bloc_screen.dart';

class HomeBlocLoadedScreen extends StatefulWidget {
  final List<MovieData> listMovieData;

  const HomeBlocLoadedScreen({Key key, this.listMovieData}) : super(key: key);

  @override
  _HomeBlocLoadedScreenState createState() => _HomeBlocLoadedScreenState();
}

class _HomeBlocLoadedScreenState extends State<HomeBlocLoadedScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBlocCubit, AuthBlocState>(
      listener: (context, state) {
        if (state is AuthBlocLoginState)
          Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text('Movie List'),
            actions: [
              IconButton(
                  icon: Icon(Icons.power_settings_new),
                  onPressed: () {
                    context.bloc<AuthBlocCubit>().logoutUser();
                  })
            ],
          ),
          body: ListView.builder(
            itemCount: widget.listMovieData.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 8.0,
                    vertical: 10.0,
                  ),
                  child: _movieDataItemWidget(widget.listMovieData[index]),
                ),
                onTap: () {
                  _movieDataItemOnTap(widget.listMovieData[index]);
                },
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _movieDataItemWidget(MovieData data) {
    return Card(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(25.0),
        ),
      ),
      child: Column(
        children: [
          Image.network(data.image.imageUrl),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Text(
              data.title,
              textDirection: TextDirection.ltr,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          )
        ],
      ),
    );
  }

  void _movieDataItemOnTap(data) => Navigator.pushNamed(
        context,
        '/detail',
        arguments: DetailBlocScreenArguments(
          homeBlocCubit: context.bloc<HomeBlocCubit>(),
          movieId: data.id,
        ),
      );
}
