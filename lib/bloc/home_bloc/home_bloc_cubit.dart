import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie/movie_data.dart';
import 'package:majootestcase/models/movie/movie_response.dart';
import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  ApiServices apiServices = ApiServices();
  MovieResponse movieResponse = MovieResponse();

  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchData() async {
    emit(HomeBlocInitialState());

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == null ||
        connectivityResult == ConnectivityResult.none) {
      emit(HomeBlocErrorConnectionState("No internet connection."));
    } else {
      movieResponse = await apiServices.getMovieList();

      if (movieResponse == null) {
        emit(HomeBlocErrorState("Unknown error."));
      } else {
        emit(HomeBlocLoadedState(movieResponse.data));
      }
    }
  }
}
