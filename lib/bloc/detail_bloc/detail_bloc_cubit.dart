import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie/movie_data.dart';
import 'package:majootestcase/models/movie/movie_response.dart';

part 'detail_bloc_state.dart';

class DetailBlocCubit extends Cubit<DetailBlocState> {
  final MovieResponse movieResponse;
  final String movieId;

  DetailBlocCubit(this.movieResponse, this.movieId)
      : super(DetailBlocInitialState());

  void fetchMovieData() async {
    emit(DetailBlocInitialState());

    MovieData data =
        movieResponse.data.firstWhere((element) => element.id == movieId);

    if (data == null) {
      emit(DetailBlocErrorState("Error Unknown"));
    } else {
      emit(DetailBlocLoadedState(data));
    }
  }
}
