import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user/user.dart';
import 'package:majootestcase/utils/global.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    emit(AuthBlocLoadingState());
    // delayed a little bit to prevent immediate state change
    await Future.delayed(Duration(milliseconds: 500));

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");

    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  Future<bool> registerUser(User user) async {
    return Future.value(await user.insert());
  }

  Future<bool> authUser(User user) async {
    final db = await database;
    List<Map<String, dynamic>> users = await db.rawQuery(
      'select case when count(*) > 0 then 1 else 0 end as auth '
      'from user '
      'where email = ? and password = ?',
      [user.email, user.password],
    );

    return Future.value(users[0]['auth'] == 1);
  }

  Future<void> loginUser(User user) async {
    String data = user.toJson().toString();

    emit(AuthBlocLoadingState());

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", true);
    await sharedPreferences.setString("user_value", data);

    emit(AuthBlocLoggedInState());
  }

  Future<void> logoutUser() async {
    emit(AuthBlocLoadingState());

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", false);
    await sharedPreferences.remove("user_value");

    emit(AuthBlocLoginState());
  }
}
