import 'dart:async';
import 'package:dio/dio.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio dioInstance;
createInstance() async {
  var options = BaseOptions(
      baseUrl: Api.BASE_URL,
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": Api.RAPIDAPI_KEY,
        "x-rapidapi-host": Api.RAPIDAPI_HOST
      });
  dioInstance = new Dio(options);
  dioInstance.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio> dio() async {
  await createInstance();
  return dioInstance;
}
