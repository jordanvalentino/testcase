import 'package:majootestcase/utils/global.dart';
import 'package:sqflite/sqflite.dart';

class User {
  String email;
  String userName;
  String password;

  User({this.email, this.userName, this.password});

  User.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() =>
      {'email': email, 'password': password, 'username': userName};

  Future<bool> insert() async {
    final db = await database;
    int id = await db.insert('user', this.toJson(),
        conflictAlgorithm: ConflictAlgorithm.abort);
    print('$id');
    return id != null;
  }
}
