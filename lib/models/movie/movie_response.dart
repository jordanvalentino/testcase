import 'package:majootestcase/models/movie/movie_data.dart';

class MovieResponse {
  List<MovieData> data;
  String query;
  int v;

  MovieResponse({
    this.data,
    this.query,
    this.v,
  });

  factory MovieResponse.fromJson(Map<String, dynamic> json) => MovieResponse(
        data: List<MovieData>.from(json["d"].map((x) => MovieData.fromJson(x))),
        query: json["q"],
        v: json["v"],
      );
  Map<String, dynamic> toJson() => {
        "d": List<dynamic>.from(data.map((x) => x.toJson())),
        "q": query,
        "v": v,
      };
}
