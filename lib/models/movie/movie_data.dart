import 'package:majootestcase/models/movie/movie_image.dart';
import 'package:majootestcase/models/movie/movie_series.dart';

class MovieData {
  MovieImage image;
  String id;
  String title;
  String q;
  int rank;
  String s;
  List<MovieSeries> series;
  int vt;
  int year;
  String yr;

  MovieData({
    this.image,
    this.id,
    this.title,
    this.q,
    this.rank,
    this.s,
    this.series,
    this.vt,
    this.year,
    this.yr,
  });

  factory MovieData.fromJson(Map<String, dynamic> json) => MovieData(
        image: MovieImage.fromJson(json["i"]),
        id: json["id"],
        title: json["l"],
        q: json["q"],
        rank: json["rank"],
        s: json["s"],
        series: json["v"] == null
            ? null
            : List<MovieSeries>.from(
                json["v"].map((x) => MovieSeries.fromJson(x))),
        vt: json["vt"] == null ? null : json["vt"],
        year: json["y"],
        yr: json["yr"] == null ? null : json["yr"],
      );
  Map<String, dynamic> toJson() => {
        "i": image.toJson(),
        "id": id,
        "l": title,
        "q": q,
        "rank": rank,
        "s": s,
        "v": series == null
            ? null
            : List<dynamic>.from(series.map((x) => x.toJson())),
        "vt": vt == null ? null : vt,
        "y": year,
        "yr": yr == null ? null : yr,
      };
}
