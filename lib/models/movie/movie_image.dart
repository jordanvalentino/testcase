class MovieImage {
  int height;
  String imageUrl;
  int width;

  MovieImage({
    this.height,
    this.imageUrl,
    this.width,
  });

  factory MovieImage.fromJson(Map<String, dynamic> json) => MovieImage(
        height: json["height"],
        imageUrl: json["imageUrl"],
        width: json["width"],
      );
  Map<String, dynamic> toJson() => {
        "height": height,
        "imageUrl": imageUrl,
        "width": width,
      };
}
