import 'package:majootestcase/models/movie/movie_image.dart';

class MovieSeries {
  MovieImage image;
  String id;
  String title;
  String s;

  MovieSeries({
    this.image,
    this.id,
    this.title,
    this.s,
  });

  factory MovieSeries.fromJson(Map<String, dynamic> json) => MovieSeries(
        image: MovieImage.fromJson(json["i"]),
        id: json["id"],
        title: json["l"],
        s: json["s"],
      );
  Map<String, dynamic> toJson() => {
        "i": image.toJson(),
        "id": id,
        "l": title,
        "s": s,
      };
}
